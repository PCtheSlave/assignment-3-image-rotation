#pragma once

#include "image.h"
#include <stdio.h>

enum readStatus {
    READ_OK = 0,
    READ_ERROR
};

enum writeStatus {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum readStatus fromBmp(FILE *file, struct image *image);

enum writeStatus toBmp(FILE *file, struct image const *image);
