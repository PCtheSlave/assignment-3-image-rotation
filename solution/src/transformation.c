#include "../include/transformation.h"

struct image rotateImage(struct image image) {
    struct image resultImage = createImage(image.height, image.width);
    if (resultImage.width != 0) {
        for (uint64_t y = 0; y < image.height; y++) {
            for (uint64_t x = 0; x < image.width; x++) {
                *getPixel(image.height - 1 - y, x, &resultImage) = *getPixel(x, y, &image);
            }
        }
    } else
        destroyImage(&resultImage);
    return resultImage;
}
