#include "../include/image.h"
#include <stdlib.h>

struct pixel *getPixel(uint64_t x, uint64_t y, const struct image *image) {
    return image->data + x + y * image->width;
}

struct image createImage(uint64_t width, uint64_t height) {
    struct pixel *data = malloc(width * height * sizeof(struct pixel));
    if (data != NULL)
        return (struct image) {width, height, data};
    else
        return (struct image) {0};
}

void destroyImage(struct image *image) {
    if (image != NULL) {
        image->width = 0;
        image->height = 0;
        free(image->data);
        image->data = NULL;
    }
}
